from json import dumps
from json import loads
from os.path import join
from os.path import dirname
from os.path import realpath
from aiofiles import open as openfile


current_dir = dirname(realpath(__file__))
input_files = {
    "PRD": join(current_dir, "production_integrations.json"),
    "SBX": join(current_dir, "sandbox_integrations.json"),
    "STG": join(current_dir, "staging_integrations.json"),
}
output_file = join(current_dir, "aggregated_integrations.json")


async def aggregate_data():
    aggregated_data = {"Apps": [], "Integrations": [], "Errors": []}

    for instance, file_path in input_files.items():
        async with openfile(file_path, mode="r") as f:
            content = await f.read()
            data = loads(content)

        for app in data.get("Apps", []):
            app["instance"] = instance
            aggregated_data["Apps"].append(app)

        for integration in data.get("Integrations", []):
            integration["instance"] = instance
            aggregated_data["Integrations"].append(integration)

        for error in data.get("Errors", []):
            error["instance"] = instance
            aggregated_data["Errors"].append(error)

    async with openfile(output_file, mode="w") as f:
        await f.write(dumps(aggregated_data, indent=2))


if __name__ == "__main__":
    from asyncio import run
    run(aggregate_data())
