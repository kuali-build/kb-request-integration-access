# Standard libraries
import os
import sys
import re
from datetime import datetime
from json import JSONDecodeError
import json
import pytz
from typing import Dict, Any, List, Set, Optional, Union
import traceback

# Third-party libraries
from loguru import logger
import aiofiles
import asyncio
from dotenv import load_dotenv
from jmespath import search as jsearch
import uvloop
import orjson
from fastapi import HTTPException

current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir)

from structures import (
    JmespathQueries,
    KualiQueries,
    KualiVariables,
    fetch_data,
)
from ldap_fetcher import LDAPDataFetcher
from combine_reports import aggregate_data
load_dotenv()


def convert_unix_to_est(unix_timestamp):
    utc_time = datetime.utcfromtimestamp(unix_timestamp / 1000)
    eastern = pytz.timezone("America/New_York")
    est_time = utc_time.replace(tzinfo=pytz.utc).astimezone(eastern)
    return est_time.strftime("%Y-%m-%d %H:%M:%S.%f")[:-3]


async def extract_data(response_json, instance):
    template = jsearch(JmespathQueries.IntegrationsQuery(), response_json)

    for app in template["Apps"]:
        app["created date"] = convert_unix_to_est(int(app["created date"]))

    return template


def generate_kuali_query(app_ids: List[str], instance: str) -> str:
    if not app_ids:
        logger.warning("No app IDs provided to generate the query.")
        return ""

    find_kuali_form_administrators = """
        {alias}: app(id: "{app_id}") {{
        listPolicyGroups {{
        id
        name
        identities {{
        type
        id
        label
        }}
        }}
        }}"""

    queries = [
        find_kuali_form_administrators.format(alias=f"_{app_id}", app_id=app_id)
        for app_id in app_ids
    ]
    return "{" + "".join(queries) + "}"


def extract_administrators_from_response(
    response: dict,
) -> Dict[str, List[Dict[str, str]]]:
    administrators = {}
    for key, app_data in response["data"].items():
        for group in app_data.get("listPolicyGroups", []):
            if group["name"] == "Administrators":
                admins_found = [
                    identity
                    for identity in group["identities"]
                    if identity["type"] == "USER"
                ]
                if not admins_found:
                    logger.warning(f"No administrators found for app_id: {key}")
                administrators[key] = admins_found
    return administrators


def generate_kuali_user_query(user_id: str) -> str:
    if not user_id:
        logger.warning("No user ID provided to generate the query.")
        return ""

    kuali_form_administrators_detail = """
        user(id: "{user_id}") {{
        name
        displayName
        schoolId
        username
        email
        }}"""

    query = kuali_form_administrators_detail.format(user_id=user_id)
    return "{" + query + "}"


async def fetch_administrators_for_apps(app_ids: List[str], instance: str):
    query = generate_kuali_query(app_ids, instance)
    if not query:
        return {}

    query_data = {"query": query}
    response_json = await fetch_data(query_data, instance)
    if "data" not in response_json:
        logger.error("Unexpected response from API: {response_json}")
        return {}

    admin_users = extract_administrators_from_response(response_json)
    unique_user_ids = set()
    for key, admin_data in admin_users.items():
        for identity in admin_data:
            unique_user_ids.add(identity["id"])

    # Fetch user details individually for each user ID
    for user_id in unique_user_ids:
        user_query = generate_kuali_user_query(user_id)
        user_query_data = {"query": user_query}
        user_response_json = await fetch_data(user_query_data, instance)
        if user_response_json is None:
            logger.error("No response received from API for user details.")
            continue

        if "data" not in user_response_json or user_response_json["data"] is None:
            logger.debug(f"Unexpected - user details: {user_response_json}")
            continue

        user_data = user_response_json["data"]["user"]
        for key, admin_data in admin_users.items():
            for identity in admin_data:
                if identity["id"] == user_id:
                    identity.update(user_data)
    return admin_users


def extract_error_data_from_response(response: dict) -> tuple:
    errors_list = []
    errors_count = {}
    for app in response["data"]["apps"]:
        app_id = app["id"]
        app_errors = []

        document_edges = app.get("documentConnection", {}).get("edges", [])
        for edge in document_edges:
            node_id = edge["node"]["id"]
            link = f"https://duke.kualibuild.com/app/builder/app/{app_id}/document/{node_id}/view"
            simulation_steps = (
                edge["node"]["meta"].get("simulation", {}).get("steps", [])
            )
            error_summaries = [
                step["msg"] for step in simulation_steps if "msg" in step
            ]
            error_messages = [
                step["error"] for step in simulation_steps if "error" in step
            ]
            statuses = [step["status"] for step in simulation_steps if "status" in step]
            stepNames = [
                step["stepName"] for step in simulation_steps if "stepName" in step
            ]
            error = {
                "id": app_id,
                "name": app["name"],
                "documentCount": app["documentCount"],
                "totalCount": app["documentConnection"]["totalCount"],
                "createdAt": edge["node"]["createdAt"],
                "node_id": node_id,
                "link": link,
                "error_summary": "\n".join(error_summaries),
                "error_messages": "\n".join(error_messages),
                "error_status": "\n".join(statuses),
                "error_step": "\n".join(stepNames),
            }
            app_errors.append(error)
        if app_errors:
            errors_list.extend(app_errors)
        errors_count[app_id] = len(app_errors) or 0
    return errors_list, errors_count


async def retrieve_count_status_data(instance):
    data_count_pending = {
        "query": KualiQueries.CountStatusQuery(),
        "variables": KualiVariables.CountPendingQuery(),
    }
    data_count_draft = {
        "query": KualiQueries.CountStatusQuery(),
        "variables": KualiVariables.CountDraftQuery(),
    }
    data_count_error = {
        "query": KualiQueries.CountStatusQuery(),
        "variables": KualiVariables.CountErrorQuery(),
    }

    (
        response_json_count_pending,
        response_json_count_draft,
        response_json_count_error,
    ) = await asyncio.gather(
        fetch_data(data_count_pending, instance),
        fetch_data(data_count_draft, instance),
        fetch_data(data_count_error, instance),
    )

    count_pending_map = {
        app["id"]: {
            "Total": app["documentCount"],
            "Pending": app["documentConnection"]["totalCount"],
        }
        for app in response_json_count_pending["data"]["apps"]
    }

    count_draft_map = {
        app["id"]: {"Draft": app["documentConnection"]["totalCount"]}
        for app in response_json_count_draft["data"]["apps"]
    }

    errors_list, errors_count = extract_error_data_from_response(
        response_json_count_error
    )

    count_error_map = {
        app_id: {"Error": count} for app_id, count in errors_count.items()
    }

    count_status_map = {
        app_id: {
            **count_pending_map.get(app_id, {}),
            **count_draft_map.get(app_id, {}),
            **count_error_map.get(app_id, {}),
        }
        for app_id in set(
            list(count_pending_map.keys())
            + list(count_draft_map.keys())
            + list(count_error_map.keys())
        )
    }

    return count_status_map, errors_list


async def retrieve_kuali_data(instance):
    all_data = {"Apps": [], "Integrations": [], "Errors": []}

    count_status_map, errors_list = await retrieve_count_status_data(instance)
    data_spaces = {"query": KualiQueries.SpaceIDs()}
    response_json_spaces = await fetch_data(data_spaces, instance)

    space_ids = [space["id"] for space in response_json_spaces["data"]["spaces"]]
    space_names = [space["name"] for space in response_json_spaces["data"]["spaces"]]

    query = KualiQueries.IntegrationsQuery()

    for app in count_status_map:
        if "Error" in app and app["Error"] > 0:
            error_data = {"id": app["id"], "name": app["name"], "errors": []}
            for edge in app["documentConnection"]["edges"]:
                error_data["errors"].append(
                    {
                        "createdAt": edge["node"]["createdAt"],
                        "node_id": edge["node"]["id"],
                    }
                )
            all_data["Errors"].append(error_data)

    async def process_space(space_id, space_name):
        data = {
            "query": query,
            "variables": {"spaceId": space_id},
        }
        response_json = await fetch_data(data, instance)
        template = await extract_data(response_json, instance)

        app_ids = [app["id"] for app in template["Apps"]]
        administrators_map = await fetch_administrators_for_apps(app_ids, instance)
        if isinstance(administrators_map, dict):
            administrators_map = {
                key.lstrip("_"): value for key, value in administrators_map.items()
            }
        else:
            logger.error(
                f"Expected administrators_map to be a dict but got: {type(administrators_map)}"
            )

        for app in template["Apps"]:
            app["Administrators"] = administrators_map.get(app["id"], [])

        unique_dukeids = {
            app.get("created by duid")
            for app in template["Apps"]
            if app.get("created by duid")
        }
        unique_schoolIds_from_admins = {
            admin["schoolId"]
            for app in template["Apps"]
            for admin in app["Administrators"]
            if "schoolId" in admin
        }
        all_unique_ids = unique_dukeids.union(unique_schoolIds_from_admins)
        ldap_fetcher = LDAPDataFetcher(list(all_unique_ids))

        all_ldap_responses = await ldap_fetcher.get_data_from_pof()
        ldap_data_map = {}
        for id, response in zip(all_unique_ids, all_ldap_responses):
            ldap_data_map[id] = response

        for app in template["Apps"]:
            app["Administrators"] = administrators_map.get(app["id"], [])

            for admin in app["Administrators"]:
                admin_schoolId = admin.get("schoolId")
                if admin_schoolId and admin_schoolId in ldap_data_map:
                    admin_ldap_response = ldap_data_map.get(admin_schoolId)
                    if isinstance(admin_ldap_response, dict):
                        admin["Full Name Exists"] = (
                            "Active"
                            if admin_ldap_response.get("Full Name")
                            else "Inactive"
                        )
                    elif isinstance(admin_ldap_response, list) and all(
                        isinstance(item, dict) for item in admin_ldap_response
                    ):
                        admin["Full Name Exists"] = (
                            "Active"
                            if any(
                                item.get("Full Name") for item in admin_ldap_response
                            )
                            else "Inactive"
                        )
                    else:
                        logger.warning(
                            f"Unexpected format for admin_ldap_response: {admin_ldap_response}"
                        )
                        admin["Full Name Exists"] = "unknown"
                else:
                    admin["Full Name Exists"] = "Not Found in LDAP"

        for app in template["Apps"]:
            app["Administrators"] = administrators_map.get(app["id"], [])
            app["space_name"] = space_name
            if app["id"] in count_status_map:
                app.update(count_status_map[app["id"]])

        for integration in template["Integrations"]:
            integration["space_name"] = space_name
            integration["space_id"] = space_id

        all_data["Integrations"].extend(template["Integrations"])
        all_data["Apps"].extend(template["Apps"])

    all_data["Errors"] = errors_list
    space_ids_names = [
        {"id": space["id"], "name": space["name"]}
        for space in response_json_spaces["data"]["spaces"]
    ]
    tasks = [process_space(space["id"], space["name"]) for space in space_ids_names]
    await asyncio.gather(*tasks)

    instance_to_file_name = {
        "PRD": "production_integrations.json",
        "SBX": "sandbox_integrations.json",
        "STG": "staging_integrations.json",
    }
    file_name = instance_to_file_name.get(instance, "default_integrations.json")

    async with aiofiles.open(
        os.path.join(current_dir, file_name), mode="wb"
    ) as json_file:
        await json_file.write(orjson.dumps(all_data, option=orjson.OPT_INDENT_2))
    logger.success(f"apps, integrations, and error data updated for {instance}")
    return all_data


async def run_three_instances():
    async with asyncio.TaskGroup() as begin:
        # get_kuali_prd_data = begin.create_task(retrieve_kuali_data(instance="PRD"))
        # get_kuali_sbx_data = begin.create_task(retrieve_kuali_data(instance="SBX"))
        get_kuali_stg_data = begin.create_task(retrieve_kuali_data(instance="STG"))


if __name__ == "__main__":
    uvloop.install()
    asyncio.run(run_three_instances())
    asyncio.run(aggregate_data())
