import sys
from loguru import logger
import time


class CircuitBreaker:
    def __init__(self, max_failures=3, reset_time=60):
        self.max_failures = max_failures
        self.reset_time = reset_time
        self.failures = 0
        self.last_failure_time = None

    def record_failure(self):
        self.failures += 1
        self.last_failure_time = time.time()
        logger.warning(f"Failure recorded. Total failures: {self.failures}.")

        if self.failures >= self.max_failures:
            logger.error("Max failures reached. Exiting program.")
            sys.exit(1)

    def record_success(self):
        if self.failures > 0:
            logger.info(f"Success recorded after {self.failures} failures.")
        else:
            logger.info("Success recorded.")
        self.failures = 0
        self.last_failure_time = None

    def can_execute(self):
        if self.failures < self.max_failures:
            return True
        if time.time() - self.last_failure_time > self.reset_time:
            self.record_success()
            return True
        logger.warning(
            f"Cannot execute. Last failure was less than {self.reset_time} seconds ago."
        )
        return False
