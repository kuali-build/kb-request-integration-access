# pof_data_fetcher.py
import httpx
import asyncio
import async_dns
from enum import Enum, auto
from jmespath import search as jsearch
from structures import CLIENT

class JmespathQueries:
    @staticmethod
    def ldap_formatter():
        return """
        output[].person[].{
            "User Attributes": [displayname[0], title[0], ou[0], mail[0]] | [?@ != null] | [join(' ⁘ ', @)] | [0],
            "Common Names": cn | [?@ != null] | [join(`; `, @)] | [0],
            "Full Name": displayname | [?@ != null] | [join(`; `, @)] | [0],
            "First Name": givenname | [?@ != null] | [join(`; `, @)] | [0],
            "Middle Initial": dumiddlename1 | [?@ != null] | [join(`; `, @)] | [0],
            "Last Name": sn | [?@ != null] | [join(`; `, @)] | [0],
            "Net ID": uid | [?@ != null] | [join(`; `, @)] | [0],
            "Duke ID": dudukeid | [?@ != null] | [join(`; `, @)] | [0],
            "Email": mail | [?@ != null] | [join(`; `, @)] | [0],
            "Title": title | [?@ != null] | [join(`; `, @)] | [0],
            "Department": ou | [?@ != null] | [join(`; `, @)] | [0],
            "LDAP Key": duldapkey | [?@ != null] | [join(`; `, @)] | [0],
            "Historical Duke ID": dudukeidhistory | [?@ != null] | [join(`; `, @)] | [0],
            "telephone number": telephonenumber | [?@ != null] | [join(`; `, @)] | [0],
            "duacmailboxexists": duacmailboxexists | [?@ != null] | [join(`; `, @)] | [0],
            "SAP Org Unit": dusaporgunit | [?@ != null] | [join(`; `, @)] | [0]
            }
        """


class LDAPDataFetcher:
    def __init__(self, searches, email=None):
        self.searches = [f"{search}" for search in searches]
        self.email = email

    async def fetch(self, url):
        return await CLIENT.get(url)

    async def get_data_for_search(self, search):
        url = f"https://pof.trinity.duke.edu/feed/ldap/default/dudukeid/{search}"
        response = await self.fetch(url)

        if response.status_code == 200:
            return jsearch(JmespathQueries.ldap_formatter(), response.json())
        else:
            raise HTTPException(status_code=400, detail="Something went wrong")

    async def get_data_from_pof(self):
        results = await asyncio.gather(
            *[self.get_data_for_search(search) for search in self.searches]
        )
        return [item for sublist in results for item in sublist]
