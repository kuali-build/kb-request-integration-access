# Standard Libs
import sys
import pwd
import socket
import os
from os.path import dirname, abspath, join, getmtime
import json
import time
from datetime import datetime, timedelta
import pytz
from typing import Optional, List
import asyncio
from asyncio import gather, all_tasks, current_task
import traceback

# External Libs
import orjson
import aiofiles
from filelock import FileLock, Timeout
import httptools
import uvloop
from pydantic import BaseModel

from fastapi import FastAPI, Depends, HTTPException, Request, Query
from fastapi import BackgroundTasks
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from fastapi.responses import JSONResponse, ORJSONResponse, HTMLResponse, FileResponse
from starlette.requests import Headers
from starlette.background import BackgroundTasks

from loguru import logger
from dotenv import load_dotenv

from apscheduler.schedulers.asyncio import AsyncIOScheduler
from apscheduler.triggers.interval import IntervalTrigger

# Helper modules
current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from kuali_integrations import run_three_instances

HOST = "0.0.0.0"
RELOAD = False
ROOT = "/kbrequestintegrationaccess"
PORT = 8109

security = HTTPBasic()
uvloop.install()
load_dotenv()

log_file = join(current_dir, "log", "kb-request-integration-access.log")
logs = logger.add(
    log_file,
    format="{time:YYYY-MM-DD at HH:mm:ss} {exception} | {level} | {message} | {line} | {module}",
    level="INFO",
    enqueue=True,
)


def verify_credentials(credentials: HTTPBasicCredentials = Depends(security)):
    correct_username = os.getenv("BASIC_AUTH_USERNAME")
    correct_password = os.getenv("BASIC_AUTH_PASSWORD")
    if (
        credentials.username != correct_username
        or credentials.password != correct_password
    ):
        raise HTTPException(status_code=401, detail="Incorrect username or password")
    return credentials


app = FastAPI(
    openapi_url=None,
    docs_url=None,
    redoc_url=None,
    default_response_class=ORJSONResponse,
)


def process_headers(headers: Headers) -> dict:
    required_headers = [
        "referer",
        "x-kuali-origin",
        "x-kuali-user-id",
        "x-kuali-user-ssoid",
    ]
    return {header: headers.get(header) for header in required_headers}


@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    headers = process_headers(request.headers)
    referer = (
        headers.get("referer")
        or f"{headers['x-kuali-origin']} | {headers['x-kuali-user-id']} | {headers['x-kuali-user-ssoid']}"
    )
    query_string = request.query_params._list

    start_time = time.time()
    try:
        response = await call_next(request)
        process_time_str = str(timedelta(seconds=time.time() - start_time))
        response.headers["X-Process-Time"] = process_time_str

        log_msg = f"{referer} | {query_string} | {process_time_str} | {request.url.path} | {response.status_code}"
        logger.success(log_msg)
        return response
    except Exception as dammit:
        error_msg = (
            f"{referer} | {query_string} | |{request.url.path} | | {str(dammit)}"
        )
        logger.error(error_msg)
        raise dammit


async def cleanup_logs(days=14):
    now = datetime.now(pytz.timezone("US/Eastern"))
    formatted_time = now.strftime("%Y-%m-%d")
    cutoff = now - timedelta(days=days)
    cutoff = cutoff.replace(tzinfo=None)

    async with aiofiles.open(log_file, mode="r") as f:
        lines = await f.readlines()

    new_lines = []
    for line in lines:
        elements = line.split(" at ", 1)
        log_time_str = " ".join(elements[:1])
        try:
            log_time = datetime.strptime(log_time_str, "%Y-%m-%d")
        except ValueError:
            continue

        if log_time > cutoff:
            new_lines.append(line)

    async with aiofiles.open(log_file, mode="w") as f:
        await f.write("".join(new_lines))


async def job():
    try:
        logger.info("Starting scheduled processes...")
        await run_three_instances()
        logger.success("Successfully completed kuali data fetching.")
    except Exception as oops:
        logger.error(f"An error occurred while fetching kuali data: {oops}")


lock = FileLock(join(current_dir, "worker_bounds.lock"))

scheduler = None


@app.on_event("startup")
async def startup_event():
    global scheduler
    try:
        with lock.acquire(timeout=1):
            await cleanup_logs()
            logger.info("Logs scrubbed")
            await run_three_instances()
            scheduler = AsyncIOScheduler()
            scheduler.add_job(job, IntervalTrigger(hours=1))
            scheduler.start()
            uid = os.getuid()
            username = pwd.getpwuid(uid).pw_name
            hostname = socket.gethostname()
            logger.info(f"Initializing on host {hostname}")
            logger.info(f"Parent directory is {parent_dir}")
            logger.info(f"Current directory is {current_dir}")
            logger.info(f"User is {username}")
            logger.success(f"Application Started on {HOST}:{PORT}")
    except Timeout:
        logger.info(
            "Could not acquire lock, another worker is likely performing this task"
        )
    except Exception as e:
        logger.error(f"An error occurred during startup: {e}\n{traceback.format_exc()}")


@app.on_event("shutdown")
async def shutdown_event():
    global scheduler
    if scheduler:
        scheduler.shutdown()
    try:
        await CLIENT.aclose()
        os.remove(join(current_dir, "worker_bounds.lock"))
    except asyncio.exceptions.CancelledError:
        logger.opt(exception=True).debug("Lock was not removed...")
    tasks = [t for t in asyncio.all_tasks() if t is not asyncio.current_task()]
    [task.cancel() for task in tasks]
    try:
        await asyncio.gather(*tasks, return_exceptions=False)
    except asyncio.exceptions.CancelledError:
        logger.opt(exception=True).debug("A task was cancelled")
    logger.success(f"Cancelled {len(tasks)} outstanding tasks")


@app.get("/data", dependencies=[Depends(verify_credentials)])
async def get(data: str = None):
    if data == "sandbox":
        json_file_path = join(current_dir, "sandbox_integrations.json")
    elif data == "staging":
        json_file_path = join(current_dir, "staging_integrations.json")
    elif data == "production":
        json_file_path = join(current_dir, "production_integrations.json")
    else:
        json_file_path = join(current_dir, "aggregated_integrations.json")
    async with aiofiles.open(json_file_path, mode="r") as f:
        content = await f.read()
    return json.loads(content)


@app.get("/dashboard/{environment}", response_class=HTMLResponse)
async def kuali_detail_dashboard(request: Request, environment: str = None):
    if environment == "sandbox":
        return FileResponse(join(current_dir, "sandbox_report.html"))
    elif environment == "staging":
        return FileResponse(join(current_dir, "staging_report.html"))
    elif environment == "production":
        return FileResponse(join(current_dir, "production_report.html"))
    else:
        return FileResponse(join(current_dir, "aggregated_report.html"))


@app.get("/environment", dependencies=[Depends(verify_credentials)])
async def select_env():
    kuali_envs = [{"env": "production"}, {"env": "sandbox"}, {"env": "staging"}]
    return ORJSONResponse(kuali_envs)


if __name__ == "__main__":
    import uvicorn
    uvicorn.run(
        f"{__name__}:app",
        host=HOST,
        port=PORT,
        proxy_headers=True,
        reload=RELOAD,
        root_path=ROOT,
        loop="uvloop",
        http="httptools",
        workers=4,
        log_level="info",
        timeout_keep_alive=int(os.environ.get("KEEP_ALIVE", 60)),
        limit_max_requests=int(os.environ.get("MAX_REQUESTS", 1000)),
    )
