import sys
import time
from loguru import logger
import httpx
import asyncio
import uvloop
import os
from os import getenv
from dotenv import load_dotenv
from fastapi import HTTPException

load_dotenv()
asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

timeouts = httpx.Timeout(120, connect=120, read=360, write=360)
CLIENT = httpx.AsyncClient(
    http2=True,
    timeout=timeouts,
    trust_env=True,
    limits=httpx.Limits(max_keepalive_connections=100, max_connections=300),
)
MAX_CONCURRENT_REQUESTS = 50
semaphore = asyncio.Semaphore(MAX_CONCURRENT_REQUESTS)

async def fetch_data(data, instance) -> dict:
    async with semaphore:
        return await fetch_json_from_url(data, instance)

class CircuitBreaker:
    def __init__(
        self, max_failures=5, reset_time=1, backoff_factor=1.5, max_attempts=10
    ):
        self.max_failures = max_failures
        self.reset_time = reset_time
        self.failures = 0
        self.last_failure_time = None
        self.backoff_factor = backoff_factor
        self.attempt = 0
        self.max_attempts = max_attempts

    async def record_failure(self):
        self.failures += 1
        self.attempt += 1
        self.last_failure_time = time.time()
        logger.warning(f"Failure recorded. Total failures: {self.failures}.")

        if self.failures >= self.max_failures:
            if self.attempt <= self.max_attempts:
                wait_time = self.reset_time * (self.backoff_factor**self.attempt)
                logger.warning(
                    f"Attempt {self.attempt} of {self.max_attempts}. Waiting for {wait_time} seconds before retry."
                )
                await asyncio.sleep(wait_time)
            else:
                logger.error(f"Max attempts ({self.max_attempts}) reached. Giving up.")

    def reset_attempts(self):
        self.attempt = 0

    def record_success(self):
        if self.failures > 0:
            logger.info(f"Success recorded after {self.failures} failures.")
        else:
            logger.info("Success recorded.")
        self.failures = 0
        self.last_failure_time = None

    def can_execute(self):
        if self.failures < self.max_failures:
            return True
        if time.time() - self.last_failure_time > self.reset_time:
            self.record_success()
            return True
        logger.warning(
            f"Cannot execute. Last failure was less than {self.reset_time} seconds ago."
        )
        return False


class Kuali:
    @staticmethod
    def _get_prefix_and_token(instance):
        prefix = getenv(f"KUALI_INSTANCE_{instance}")
        auth_token = getenv(f"AUTH_TOKEN_{instance}")
        return prefix, auth_token

    @staticmethod
    def get_url_and_headers(instance):
        prefix, auth_token = Kuali._get_prefix_and_token(instance)
        url = f"https://{prefix}.kualibuild.com/app/api/v0/graphql"
        headers = {
            "Accept-Encoding": "gzip, deflate, br",
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Connection": "keep-alive",
            "DNT": "1",
            "Origin": f"https://{prefix}.kualibuild.com",
            "Authorization": auth_token,
        }
        return url, headers


# T-safe counter
class Counter:
    def __init__(self):
        self._count = 0
        self._lock = asyncio.Lock()

    async def increment(self):
        async with self._lock:
            self._count += 1
            return self._count


_api_call_count = Counter()
_circuit_breakers = {}


async def ensure_circuit_breaker(instance):
    if instance not in _circuit_breakers:
        _circuit_breakers[instance] = CircuitBreaker()
    return _circuit_breakers[instance]


async def execute_request(url, headers, data, circuit_breaker):
    response = await CLIENT.post(url, headers=headers, json=data)
    response.raise_for_status()
    circuit_breaker.reset_attempts()
    return response.json()


async def handle_error(turd, circuit_breaker):
    if turd.response.status_code != 500:
        raise HTTPException(
            status_code=500, detail="Failed to process the Kuali query"
        ) from turd

    await circuit_breaker.record_failure()


async def fetch_json_from_url(data, instance) -> dict:
    api_count = await _api_call_count.increment()
    logger.info(f"API 📞 #{api_count}")

    circuit_breaker = await ensure_circuit_breaker(instance)

    for _ in range(circuit_breaker.max_attempts):
        if not circuit_breaker.can_execute():
            raise HTTPException(
                status_code=503,
                detail="Service temporarily unavailable due to multiple failures",
            )

        url, headers = Kuali.get_url_and_headers(instance)

        try:
            return await execute_request(url, headers, data, circuit_breaker)
        except httpx.HTTPStatusError as turd:
            await handle_error(turd, circuit_breaker)

    raise HTTPException(
        status_code=500,
        detail="Failed to process the Kuali query after multiple attempts",
    )


class JmespathQueries:
    @staticmethod
    def IntegrationsQuery():
        return """
            {
            "Apps": data.space.apps[].{
            "id": id,
            "name": name,
            "created date": createdAt,
            "created by id": createdBy.id,
            "created by name": createdBy.name,
            "created by duid": createdBy.schoolId,
            "created by username": createdBy.username
            "Administrators": []
            },
            "Integrations": data.integrationsConnection.edges[].{
            "name": node.name,
            "id": node.id,
            "space_name": "",
            "space_id": "",
            "description": node.data.__description,
            "type": node.data.__type.label,
            "method": node.data.__method.label,
            "URL": node.data.__url,
            "authentication": node.data.__authenticationType.label,
            "id key": node.data.__idKey,
            "label key": node.data.__labelKey,
            "input fields": node.data.__queryParameterInputs[].__label | [?@ != null] | [join(`; `, @)] | [0],
            "output fields": node.data.__outputs[].__label | [?@ != null] | [join(`; `, @)] | [0],
            "linked gadgets": node.data.__outputs[].__gadget.type.label | [?@ != null] | [join(`; `, @)] | [0],
            "app owners": node.appsUsing[].{
            "form": name,
            "documents": documentCount,
            "owner": createdBy.displayName,
            "typeahead": {
            "id": createdBy.id,
            "label": createdBy.username
            }
            },
            "share_integration_executable": {
            "id": node.id,
            "sharingType": node.sharedWithOthers.type,
            "apps": node.appsUsing[].{
            "id": id,
            "label": name
            }
            }
            }
            }
            """


class KualiQueries:
    @staticmethod
    def SpaceIDs():
        return """
            query KualiSpaceIDs {
            spaces {
                id
                name
            }
            }
        """

    @staticmethod
    def IntegrationsQuery():
        return """
        query IntegrationsQuery($spaceId: ID, $sort: String) {
        space(id: $spaceId) {
        name
        apps {
        name
        id
        createdAt
        createdBy {
        name
        id
        schoolId
        username
        ssoId
        }
        }
        }
        integrationsConnection(spaceId: $spaceId, args: { sort: $sort }) {
        edges {
        node {
        name
        sharedWithOthers {
        type
        }
        id
        data
        appsUsing {
        name
        id
        documentCount
        createdBy {
        displayName
        id
        username
        }
        }
        }
        }
        }
        }
        """

    @staticmethod
    def CountStatusQuery():
        return """
        query apps($skip: Int!, $limit: Int!, $fields: Operator) {
        apps {
        id
        name
        documentCount
        documentConnection(
        args: { skip: $skip, limit: $limit, fields: $fields }
        keyBy: ID
        ) {
        totalCount
        edges {
        node {
        createdAt
        id
        meta
        }
        }
        }
        }
        }
        """

    @staticmethod
    def TotalStatusQuery():
        return """
        query ListPageQuery(
        $appId: ID!
        $pageId: ID
        $skip: Int!
        $limit: Int!
        $fields: Operator
        ) {
        app(id: $appId) {
        id
        name
        dataset(id: $pageId) {
        isPublished
        documentConnection(
        args: {
        skip: $skip
        limit: $limit
        fields: $fields
        }
        keyBy: ID
        ) {
        totalCount
        }
        }
        }
        }
        """

    @staticmethod
    def StatusQuery():
        return """
        query ListPageQuery($appId: ID!, $pageId: ID, $skip: Int!, $limit: Int!, $sort: [String!], $query: String, $fields: Operator) {
        app(id: $appId) {
        id
        name
        dataset(id: $pageId) {
        id
        isPublished
        documentConnection(
        args: {skip: $skip, limit: $limit, sort: $sort, query: $query, fields: $fields}
        keyBy: ID
        ) {
        totalCount
        edges {
        node {
        id
        createdAt
        viewer {
        canEdit
        canDelete
        }
        meta
        }
        }
        pageInfo {
        hasNextPage
        hasPreviousPage
        skip
        limit
        }
        }
        }
        }
        }
        """


class KualiVariables:
    @staticmethod
    def TotalStatusQuery(app_id):
        return {
            "appId": app_id,
            "skip": 0,
            "limit": 1,
            "fields": {
                "type": "OR",
                "operators": [
                    {
                        "field": "meta.workflowStatus",
                        "type": "IS",
                        "value": "In Progress",
                    },
                    {"field": "meta.workflowStatus", "type": "IS", "value": "Draft"},
                ],
            },
        }

    @staticmethod
    def CountStatusQuery():
        return {
            "skip": 0,
            "limit": 350,
            "fields": {
                "type": "OR",
                "operators": [
                    {
                        "field": "meta.workflowStatus",
                        "type": "IS",
                        "value": "In Progress",
                    },
                    {"field": "meta.workflowStatus", "type": "IS", "value": "Draft"},
                ],
            },
        }

    @staticmethod
    def CountPendingQuery():
        return {
            "skip": 0,
            "limit": 350,
            "fields": {
                "type": "OR",
                "operators": [
                    {
                        "field": "meta.workflowStatus",
                        "type": "IS",
                        "value": "In Progress",
                    }
                ],
            },
        }

    @staticmethod
    def CountDraftQuery():
        return {
            "skip": 0,
            "limit": 350,
            "fields": {
                "type": "OR",
                "operators": [
                    {"field": "meta.workflowStatus", "type": "IS", "value": "Draft"}
                ],
            },
        }

    @staticmethod
    def CountErrorQuery():
        return {
            "skip": 0,
            "limit": 350,
            "fields": {
                "type": "OR",
                "operators": [
                    {"field": "meta.workflowStatus", "type": "IS", "value": "Error"}
                ],
            },
        }

    @staticmethod
    def StatusQuery(app_id):
        return {
            "appId": app_id,
            "skip": 0,
            "limit": 25,
            "sort": ["-meta.serialNumber"],
            "query": "",
            "fields": {
                "type": "OR",
                "operators": [
                    {
                        "field": "meta.workflowStatus",
                        "type": "IS",
                        "value": "In Progress",
                    },
                    {"field": "meta.workflowStatus", "type": "IS", "value": "Draft"},
                ],
            },
        }
